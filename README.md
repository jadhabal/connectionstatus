# ConnectionStatus Class iOS 10 Swift 3#

Here is a very simple way to tell when you're using 3G/4G or whatever mobile data or Wifi 

```
#!swift

import SystemConfiguration


protocol ConnectionStatus {
}

extension NSObject:ConnectionStatus{


    enum ReachabilityStatus {
        case notReachable
        case reachableViaMobileData
        case reachableViaWiFi
    }

    var currentReachabilityStatus: ReachabilityStatus {

        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }

        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaMobileData
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        } 
        else {
            return .notReachable
        }
    }

}
```

### How do I get set up? ###

* Download ConnectionStatus protocol
* Drag & drop into your project
* Use it anywhere in your project as illustrated below;



```
#!swift

func viewDidLoad(){
      print(currentReachabilityStatus != .notReachable) //true connected
}
```

or 


```
#!swift

switch currentReachabilityStatus {
case .notReachable:
    print("Not connected to internet")
case .reachableViaWiFi:
    print("connected to internet via Wifi")
case .reachableViaMobileData:
    print("connected to internet via Mobile Data")
}
```


You can also add Utilities as delegate to anything in your project.